'use strict';
const AWS_REGION = 'ap-northeast-1';
const ID_POOL_ID = 'ap-northeast-1:e91790f1-0d00-4fa2-a040-d3f1ab979117';
// const USER_POOL_ID = ;
// const CLIENT_ID_BROWSER = ;
// const CLIENT_ID_COGNITO = ;

//credential初期化
function initAWSCredentail(){
    return new Promise(function(resolve,reject){
        AWS.config.region = AWS_REGION; // Region
        AWS.config.credentials = new AWS.CognitoIdentityCredentials({IdentityPoolId: ID_POOL_ID});
        AWS.config.credentials.get(function(err){
            if(err){
                reject(err);
            }else{
                resolve();
            }
        });
    });
}
function initAWSCognitoCredential(){
    AWS.config.region = AWS_REGION; // Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({IdentityPoolId: ID_POOL_ID});
    AWSCognito.config.region = AWS_REGION;
    AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({IdentityPoolId : ID_POOL_ID});
}

//APIGatewayの初期化
function initAPIClient(){
    return new Promise(function(resolve, reject){
        AWS.config.region = AWS_REGION; // Region
        AWS.config.credentials = new AWS.CognitoIdentityCredentials({IdentityPoolId: ID_POOL_ID});
        AWS.config.credentials.get(function(err){
            if(err){
                reject(err);
            }else{
                console.log('AWS Credentail OK:', AWS.config.credentials.identityId);
                const apigClient = apigClientFactory.newClient({
                    accessKey: AWS.config.credentials.accessKeyId,
                    secretKey: AWS.config.credentials.secretAccessKey,
                    sessionToken: AWS.config.credentials.sessionToken,
                    region: AWS_REGION
                });
                resolve(apigClient);
            }
        });
    });
};

function signUpToUserPool(username, password, attributeList){
    return new Promise(function(resolve, reject){
        const data = {
            UserPoolId: USER_POOL_ID,
            ClientId: CLIENT_ID_BROWSER
        };
        const userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(data);
        userPool.signUp(username, password, attributeList, null, function(err, result){
            if(err){
                reject(err);
            }else{
                resolve(result);
            }
        });
    });
}
function addAttribute(attributeName, attributeValue){
    const data = {
        Name: attributeName,
        Value: attributeValue
    };
    return new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(data);
}
function login(username, password, newpassword1, newpassword2){
    return new Promise(function(resolve,reject){
        const data = {
            UserPoolId: USER_POOL_ID,
            ClientId: CLIENT_ID_COGNITO,
            Paranoia: 7
        };
        const userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(data);
        const userData = {
            Username: username,
            Pool: userPool
        };
        const cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        const authData = {
            Username: username,
            Password: password
        };
        const authDetails = new AWSCognito.CognitoIdentityServiceProvider.AuthenticationDetails(authData);
        cognitoUser.authenticateUser(authDetails,{
            onSuccess: function(result){
                resolve(result);
            },
            onFailure: function(err){
                reject(err);
            },
            newPasswordRequired: function(userAttributes, requiredAttributes){
                // FORCE_CHANGE_PASSWORDステータスのユーザーが予め設定されたパスワードでログインをトライした場合
                if(newpassword1 != '' && newpassword2 != '' && newpassword1 == newpassword2){
                    cognitoUser.completeNewPasswordChallenge(newpassword1, null, {
                        onSuccess: function(result){
                            resolve(result);
                        },
                        onFailure: function(err){
                            reject(err);
                        }
                    });
                }else if(newpassword1 != '' && newpassword2 != ''){
                    const err = new Error();
                    err.name = 'NewPasswordNotMatch';
                    err.message = 'New passwords do not match';
                    reject(err);
                }else{
                    const err = new Error();
                    err.name = 'NewPasswordRequired';
                    err.message = 'Need to set a new password for first login';
                    reject(err);
                }
            }
        });
    });
}

function setNewPassword(cognitoUser, newpassword){
  return new Promise(function(resolve, reject){
    cognitoUser.completeNewPasswordChallenge(newpassword, null, {
      onSuccess: function(result){
        resolve(result);
      },
      onFailure: function(err){
        reject(err);
      }
    });
  });
}

function logout(path){
    getCurrentCognitoUser()
    .then(function(cognitoUser){
        cognitoUser.signOut();
        AWS.config.credentials.clearCachedId();
    });
    window.location.href = path;
}
function validateSession(){
    return new Promise(function(resolve,reject){
        Promise.resolve()
        .then(function(){
            return getCurrentCognitoUser();
        })
        .then(function(cognitoUser){
            return getCurrentSession(cognitoUser);
        })
        .then(function(sessresult){
            return getCurrentCredential(sessresult);
        })
        .then(function(){
            resolve();
        })
        .catch(function(err){
            console.log(err);
            reject(new Error('SessionExpired'));
        });
    });
}

function getCurrentCognitoUser(){
    return new Promise(function(resolve,reject){
        AWS.config.region = AWS_REGION;
        const data = {
            UserPoolId: USER_POOL_ID,
            ClientId: CLIENT_ID_COGNITO,
            Paranoia: 7
        };
        const userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(data);
        const cognitoUser = userPool.getCurrentUser();
        if(cognitoUser != null){
            resolve(cognitoUser);
        }else{
            reject(new Error('No Session'));
        }
    });
}
function getCurrentSession(cognitoUser){
    return new Promise(function(resolve, reject){
        cognitoUser.getSession(function(err, sessresult){
            if(sessresult){
                resolve(sessresult);
            }else{
                reject(new Error('No Session'));
            }
        });
    });
}
function getCurrentCredential(sessresult){
    return new Promise(function(resolve,reject){
        let loginObj = {};
        loginObj['cognito-idp.' + AWS_REGION + '.amazonaws.com/' + USER_POOL_ID] = sessresult.getIdToken().getJwtToken();
        const cognitoParams = {
            IdentityPoolId: ID_POOL_ID,
            Logins: loginObj
        };
        AWS.config.credentials = new AWS.CognitoIdentityCredentials(cognitoParams);
        AWS.config.credentials.get(function(err){
            if(err){
                reject(new Error('No Session'));
            }else{
                console.log('AWS AuthCredentail OK:', AWS.config.credentials.identityId);
                resolve();
            }
        });
    });
}