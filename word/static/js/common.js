'use strict';
function getAWSIdentityId(){
  return AWS.config.credentials.identityId;
}

function makeInternationalPhoneNumber(prefix, nationCode, number){
  let fullPhoneNumber = '';
  if(number.slice(0,1) == '0' && nationCode != 'IT'){
    number = number.slice(1).replace(/-/g, '');
  }else{
    number = number.replace(/-/g, '');
  }
  fullPhoneNumber = prefix + number;
  fullPhoneNumber = fullPhoneNumber.replace(/\s/g, '');
  return fullPhoneNumber;
}

//フォームのvalidation
function formValidation(form){
  $(form).find('input').each(function(index, elem){
    if($(elem).attr('required') == 'required' && $(elem).val() == ''){
      $(elem).addClass('has-error');
      throw new Error('shortInput');
    }else if($(elem).attr('required') == 'required' && $(elem).attr('type') == 'email' && $(elem).val().match(/^[A-Za-z0-9]+[\w-]+@[\w\.-]+\.\w{2,}$/) == null){
      $(elem).addClass('has-error');
      throw new Error('incorrectEmail');
    }else if($(elem).attr('required') == 'required' && $(elem).attr('type') == 'tel' && $(elem).val().match(/^(0[5-9]0[0-9]{8}|0[1-9][1-9][0-9]{7})$/) == null){
      $(elem).addClass('has-error');
      throw new Error('incorrectTel');
    }else{
      $(elem).removeClass('has-error');
    }
  });
  $(form).find('select').each(function(index, elem){
    if($(elem).attr('required') == 'required' && $(elem).val() == ''){
      console.log($(elem));
      $(elem).addClass('has-error');
      throw new Error('shortSelect');
    }
  });

  return true;
  /*
  {
  shortInput: "入力項目が不足しています",
  incorrectEmail: "メールアドレスの形式が不正です",
  incorrectTel: "電話番号の形式が不正です",
  }
  */
}

//アラートを表示する
function showEasyAlert(text){
  //<div id="alert" class="alert alert-danger" role="alert" style="display: none;"></div>
  $('#alert').text(text);
  $('#alert').show();
  // setTimeout(function(){
  //   $('#alert').fadeOut();
  // }, 2000);
};
function showAlert(text, message, type){
  swal({
    title: text,
    text: message,
    type: type,
    confirmButtonText: "OK",
    confirmButtonColor: "#2c3e50"
  });
}
function showAlertWithExe(text, message, type, execute){
  swal({
    title: text,
    text: message,
    type: type,
    confirmButtonText: "OK",
    confirmButtonColor: "#2c3e50",
    closeOnConfirm: false
    },
    function(){
      execute();
    });
}
function showConfimAlert(text, message, type, button, execute){
  swal({
    title: text,
    text: message,
    type: type,
    allowOutsideClick: true,
    showCancelButton: true,
    confirmButtonColor: "#428bca",
    confirmButtonText: button,
    cancelButtonText: "Cancel",
    showLoaderOnConfirm: false,
    closeOnConfirm: true
    },
    function(){
      execute();
    });
}
function showConfimAlertWithLoading(text, message, type, button, execute){
  swal({
    title: text,
    text: message,
    type: type,
    allowOutsideClick: true,
    showCancelButton: true,
    confirmButtonColor: "#428bca",
    confirmButtonText: button,
    cancelButtonText: "Cancel",
    showLoaderOnConfirm: true,
    closeOnConfirm: false
    },
    function(){
      execute();
    });
}
//URLパラメータを取得する
function getURLParam(name){
  let returnStr = ""
  const urlStr = location.search.substring(1);
  const urlParam = urlStr.split('&');
  if(urlParam.length > 0){
    urlParam.forEach(function(str,index){
      const paramItem = str.split('=');
      if(paramItem[0] == name){
        returnStr = paramItem[1];
      }
    });
  }
  return returnStr;
}
//日付操作系

function getStringDate(unix){
  const weekday = ['日', '月', '火', '水', '木', '金', '土'];
  const date = new Date(unix * 1000);
  const month = String(date.getMonth() + 1);
  const day = String(date.getDate());
  const week = weekday[date.getDay()];
  return month + '月' + day + '日(' + week + ')';
}
function getStringDateTime(unix){
  const weekday = ['日', '月', '火', '水', '木', '金', '土'];
  const date = new Date(unix * 1000);
  const month = String(date.getMonth() + 1);
  const day = String(date.getDate());
  const week = weekday[date.getDay()];
  const hour = addZero(String(date.getHours()));
  const min = addZero(String(date.getMinutes()));
  return month + '月' + day + '日(' + week + ') ' + hour + ':' + min;
}
function getFullStringDate(startUnix, endUnix){
  const weekday = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
  const startDate = new Date(startUnix * 1000);
  const endDate = new Date(endUnix * 1000);
  const month = String(startDate.getMonth() + 1);
  const day = String(startDate.getDate());
  const week = weekday[startDate.getDay()];
  let startHour = addZero(String(startDate.getHours()));
  let startMin = addZero(String(startDate.getMinutes()));
  let endHour = addZero(String(endDate.getHours()));
  let endMin = addZero(String(endDate.getMinutes()));
  return month + '/' + day + ' (' + week + ') ' + startHour + ':' + startMin + '~' + endHour + ':' + endMin;
}
function addZero(str){
  if(str.length == 1){
    return '0' + str;
  }else{
    return str;
  }
}
function getFlatUnix(unix){
  const date = new Date(unix * 1000);
  const hour = date.getHours();
  const min = date.getMinutes();
  const sec = date.getSeconds();
  return unix - hour * 60 * 60 - min * 60 - sec;
}

function getDateObj(unix){
  const week = ['月', '火', '水', '木', '金', '土', '日'];
  let date;
  if(unix){
    date = new Date(unix * 1000);
  }else{
    date = new Date();
  }
  const weekdayIndex = date.getDay() == 0 ? 6 : date.getDay() - 1
  return {
    unix: Math.floor(date.getTime() / 1000),
    year: date.getFullYear(),
    month: date.getMonth() + 1,
    day: date.getDate(),
    weekdayIndex: weekdayIndex,
    hour: date.getHours(),
    min: date.getMinutes(),
    sec: date.getSeconds(),
    flatUnix: Math.floor(date.getTime() / 1000) - date.getHours() * 60 * 60 - date.getMinutes() * 60 - date.getSeconds(),
    weekTopFlatUnix : Math.floor(date.getTime() / 1000) - date.getHours() * 60 * 60 - date.getMinutes() * 60 - date.getSeconds() - weekdayIndex * 24 * 60 * 60,
    stringYear: String(date.getFullYear()) + '年',
    stringMonth: String(date.getMonth() + 1) + '月',
    stringDay: String(date.getDate()) + '日',
    stringWeek: week[weekdayIndex],
    stringHour: zeroFill(String(date.getHours())),
    stringMin: zeroFill(String(date.getMinutes())),
    stringSec: zeroFill(String(date.getSeconds())),
    dateString: String(date.getFullYear()) + '-' + zeroFill(String(date.getMonth() + 1)) + '-' + zeroFill(String(date.getDate())),
    timeString: zeroFill(String(date.getHours())) + ':' + zeroFill(String(date.getMinutes()))
  };
}

function getNow(){
  const date = new Date();
  return Math.floor(date.getTime() / 1000);
}
function timestampMilFunc(){
  const date = new Date();
  return date.getTime();
}
function convertUnix(dateString, timeString){
    //dateString 2017-07-14
    //timeString 18:00
    dateString = dateString.replace(/-/g, '/');
    timeString = timeString + ':00';
    const date = new Date(dateString + ' ' + timeString);
    return Math.floor(date.getTime() / 1000);
}

function convertUnixTimeOnly(timeString){
  //timeStrng: 09:00
  const timeUnix = Number(timeString.split(':')[0]) * 60 * 60 + Number(timeString.split(':')[1]) * 60;
  if(!isNaN(timeUnix)){
    return timeUnix;
  }else{
    throw new Error('InvaildTime');
  }
}
function convertTimeString(timeUnix){
  const hour = Math.floor(timeUnix / (60 * 60));
  const min = Math.floor( (timeUnix - hour * 60 * 60) / 60 );
  return addZero(String(hour)) + ':' + addZero(String(min));
}
//エスケープ
function escapeUtil(text){
  text = _.escape(text);
  const exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
  text = text.replace(exp, "<a href='$1' target='_blank'>$1</a>");//リンクを有効化
  text = text.replace(/\r?\n/g, '<br>');//改行コードを変換
  return text;
}
function zeroFill(str){
  if(str.length == 1){
    return '0' + str;
  }else{
    return str;
  }
}
