'use strict';

module.exports = function(){

    const week = ['月', '火', '水', '木', '金', '土', '日'];
    const zeroFill = function(str){
        if(str.length == 1){
            return '0' + str;
        }else{
            return str;
        }
    };

    this.ramdomArry = function(array){
        for (var i = array.length - 1; i >= 0; i--){
            // 0~iのランダムな数値を取得
            var rand = Math.floor( Math.random() * ( i + 1 ) );
            // 配列の数値を入れ替える
            [array[i], array[rand]] = [array[rand], array[i]];
        }
        return array;
    }

    this.generateCode = function(){
        const val = Math.floor(Math.random() * 999999) + 100000;
        return val + '';
    };

    this.timestampFunc = function (){
        const date = new Date();
        return Math.floor(date.getTime() / 1000);
    };

    this.getDateObj = function (unix){
        let date;
        if(unix){
            date = new Date(unix * 1000);
        }else{
            date = new Date();
        }
        const week_index = date.getDay() == 0 ? 6 : date.getDay() - 1;
        return {
            unix: Math.floor(date.getTime() / 1000),
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate(),
            week_index: week_index,
            hour: date.getHours(),
            min: date.getMinutes(),
            sec: date.getSeconds(),
            flat_unix: Math.floor(date.getTime() / 1000) - date.getHours() * 60 * 60 - date.getMinutes() * 60 - date.getSeconds(),
            stringYear: String(date.getFullYear()) + '年',
            stringMonth: String(date.getMonth() + 1) + '月',
            stringDay: String(date.getDate()) + '日',
            stringWeek: week[week_index],
            stringHour: zeroFill(String(date.getHours())),
            stringMin: zeroFill(String(date.getMinutes())),
            stringSec: zeroFill(String(date.getSeconds()))
        };
    };

    this.getFullStringDate = function(startUnix, endUnix){
        if(endUnix){
            const startObj = this.getDateObj(startUnix);
            const endObj = this.getDateObj(endUnix);
            if(startObj.year != endObj.year){//年をまたいでいる場合
                return startObj.stringYear + startObj.stringMonth + startObj.stringDay + '（' + startObj.stringWeek + '） ' + startObj.stringHour + ':' + startObj.stringMin + '~' + endObj.stringYear + endObj.stringMonth + endObj.stringDay + '（' + endObj.stringWeek + '） ' + endObj.stringHour + ':' + endObj.stringMin;
            }else if(startObj.month != endObj.month || startObj.day != endObj.day){//日をまたいでいる場合
                return startObj.stringYear + startObj.stringMonth + startObj.stringDay + '（' + startObj.stringWeek + '） ' + startObj.stringHour + ':' + startObj.stringMin + '~' + endObj.stringMonth + endObj.stringDay + '（' + endObj.stringWeek + '） ' + endObj.stringHour + ':' + endObj.stringMin;
            }else{//日をまたいでいない
                return startObj.stringYear + startObj.stringMonth + startObj.stringDay + '（' + startObj.stringWeek + '） ' + startObj.stringHour + ':' + startObj.stringMin + '~' + endObj.stringHour + ':' + endObj.stringMin;
            }
        }else{
            const startObj = this.getDateObj(startUnix);
            return startObj.stringYear + startObj.stringMonth + startObj.stringDay + '（' + startObj.stringWeek + '） ' + startObj.stringHour + ':' + startObj.stringMin;
        }
    };

    this.compareValues = function(key, order='asc') {
        return function(a, b) {
            if(!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
                // property doesn't exist on either object
                return 0; 
            }

            const varA = (typeof a[key] === 'string') ? a[key].toUpperCase() : a[key];
            const varB = (typeof b[key] === 'string') ? b[key].toUpperCase() : b[key];

            let comparison = 0;
            if (varA > varB) {
                comparison = 1;
            } else if (varA < varB) {
                comparison = -1;
            }
            return (
                (order == 'desc') ? (comparison * -1) : comparison
            );
        };
    };

    this.getAvailableDates = function(raw_events, period_start, period_end, min , max){
        console.log('RAW EVENTS:', raw_events.length);
        let available = {};
        let unavailable = {};
        const events = raw_events.map((event) => {
            const start = new Date(event.startUnix * 1000);
            const end = new Date(event.endUnix * 1000);
            return {
                start_week_index: start.getDay() == 0 ? 6 : start.getDay() - 1,
                end_week_index: end.getDay() == 0 ? 6 : end.getDay() - 1,
                start_unix: Math.floor(start.getTime() / 1000),
                end_unix: Math.floor(end.getTime() / 1000),
                start_flat_unix: Math.floor(start.getTime() / 1000) - start.getHours() * 60 * 60 - start.getMinutes() * 60 - start.getSeconds(),
                end_falt_unix: Math.floor(end.getTime() / 1000) - end.getHours() * 60 * 60 - end.getMinutes() * 60 - end.getSeconds()
            };
        });
        console.log('EVENTS:', events.length);
        events.sort(this.compareValues('start_unix'));
        console.log('SORTED EVENTS:', events.length);
        events.forEach((event, index) => {
            if( min && max && ( event.end_unix <= min || event.start_unix >= max )){
                //不要な期間の情報を返さない
            }else{
                const length = Math.ceil((event.end_unix - event.start_unix) / (24 * 60 * 60));
                for(var i = 0; i < length; i++){
                    if(!unavailable[event.start_flat_unix + i * 24 * 60 * 60]){
                        unavailable[event.start_flat_unix + i * 24 * 60 * 60] = [];
                    }
                    if(!available[event.start_flat_unix + i * 24 * 60 * 60]){
                        available[event.start_flat_unix + i * 24 * 60 * 60] = [{startUnix: event.start_flat_unix + period_start, endUnix: event.start_flat_unix + period_end}];
                    }
                    unavailable[event.start_flat_unix + i * 24 * 60 * 60].push(event);
                }
            }
        });
        console.log('Unavailable:', unavailable);
        Object.keys(unavailable).forEach((flat_unix) => {
            let start_time = Number(flat_unix) + period_start;
            let end_time = Number(flat_unix) + period_end;
            unavailable[flat_unix].forEach((event, index) => {
                // console.log("予定日時:", this.getFullStringDate(event.start_unix, event.end_unix));
                const length = available[flat_unix].length;
                if( min && max && (event.end_unix <= min || event.start_unix >= max)){
                    //不要な期間の情報を返さない
                }else if(event.start_unix <= start_time && event.end_unix >= end_time){
                    //イベントが計算時間帯を覆い尽くしているケース
                    available[flat_unix][length - 1].startUnix = end_time;
                }else if(event.start_unix <= start_time && event.end_unix > start_time && event.end_unix < end_time){
                    //イベントが計算時間帯の前部分を覆っているケース
                    start_time = event.end_unix;
                    available[flat_unix][length - 1].startUnix = start_time;
                }else if(event.start_unix > start_time && event.end_unix < end_time){
                    //イベントが計算時間帯を中抜けさせるケース
                    start_time = event.end_unix;
                    available[flat_unix][length - 1].endUnix = event.start_unix;
                    available[flat_unix].push({
                        startUnix: start_time,
                        endUnix: end_time
                    });
                }else if(event.start_unix > start_time && event.start_unix < end_time && event.end_unix >= end_time){
                    //イベントが計算時間帯の後半部分を覆っているケース
                    start_time = end_time;
                    available[flat_unix][length - 1].endUnix = event.start_unix;
                }
            });
        });
        console.log('Available:', available);
        return available;
    };

};