'use strict';
const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();

module.exports = function(){

    this.getItem = function (table, key){
        return new Promise(function(resolve, reject){
            const params = {
                TableName: table,
                Key: key
            };
            docClient.get(params, function(err,data){
                if(err){
                    reject(err);
                }else{
                    resolve(data.Item);
                }
            });
        });
    };

    this.putItem = function(table, item, condition){
        return new Promise(function(resolve, reject){
            let newItem = {};
            Object.keys(item).forEach(function(key){
                if(item[key] != '' || typeof item[key] === 'boolean'){
                    newItem[key] = item[key];
                }
            });
            let params = {
                TableName: table,
                Item: newItem
            };
            if(condition){
                params['ConditionExpression'] = condition;
            }
            docClient.put(params, function(err, data){
                if(err){
                    reject(err);
                }else{
                    resolve(data);
                }
            });
        });
    };

    this.scan = function(table, filterExpression){
        return new Promise(function(resolve, reject){
            let params = {TableName: table};
            if(filterExpression){
                params['FilterExpression'] = filterExpression;
            }
            docClient.scan(params, function(resolve,reject){
                if(err){
                    reject(err);
                }else{
                    resolve(data);
                }
            });
        });
    };

    this.query = function(table, index ,condition, projection){
        /*
        condition = {
            'primaryKey': {
                'name': keyname,
                'value': value
            },
            'sortKey': {
                'name': keyname,
                'value': value,
                'condition': condition //'=', '<', '>', '<=', '>='
            },
            'filter': [
                {
                    'name': keyname,
                    'value': value,
                    'condition': condition //'=', '<', '>', '<=', '>='
                },,,
            ]
        }
        projection = ProjectionExpression
        */
        return new Promise(function(resolve, reject){
            //テーブル名を設定
            let params = {TableName: table};
            //インデックスがあったら追加
            if(index){
                params['IndexName'] = index;
            }
            let eanObj = {};//Expression Attribute Names
            let eavObj = {};//Expression Attribute Values
            let kceString = '';//KeyCondition Expression
            eanObj['#' + condition.primaryKey.name] = condition.primaryKey.name;
            eavObj[':' + condition.primaryKey.name] = condition.primaryKey.value;
            kceString = '#' + condition.primaryKey.name + ' = :' + condition.primaryKey.name;
            if(condition.sortKey){
                eanObj['#' + condition.sortKey.name] = condition.sortKey.name;
                eavObj[':' + condition.sortKey.name] = condition.sortKey.value;
                kceString =  kceString + ' AND #' + condition.sortKey.name + ' ' + condition.sortKey.condition + ' :' + condition.sortKey.name;
            }
            params['KeyConditionExpression'] = kceString;
            if(condition.filter && condition.filter.length > 0){
                let feString = ''//FilterExpression
                condition.filter.forEach(function(filter){
                    eanObj['#' + filter.name] = filter.name;
                    eavObj[':' + filter.name] = filter.value;
                    feString = feString + '#' + filter.name + ' ' + filter.condition + ' :' + filter.name + ' AND';
                });
                feString = feString.substring(0, feString.length - 4);//最後の' AND'を削除
                params['FilterExpression'] = feString;
            }
            if(projection){
                params['ProjectionExpression'] = projection;
            }
            params['ExpressionAttributeNames'] = eanObj;
            params['ExpressionAttributeValues'] = eavObj;
            console.log('DynamoDB Query:', params);
            docClient.query(params, function(err,data){
                if(err){
                    reject(err);
                }else{
                    resolve(data.Items);
                }
            });
        });
    };

    this.deleteItem = function(table, key){
        return new Promise(function(resolve, reject){
            const params = {
                TableName: table,
                Key: key
            };
            docClient.delete(params, function(err,data){
                if(err){
                    reject(err);
                }else{
                    resolve(data);
                }
            });
        });
    }

    this.updateAttribute = function(table, key, attributeObj){
        //key = {primaryKey: value, sortKey: value}
        //attributeObj = {name: value}
        return new Promise(function(resolve,reject){
            let eanObj = {};
            let eavObj = {};
            let stString = 'SET ';
            let rmString = 'REMOVE ';
            let ueString = '';
            let stCount = 0;
            let rmCount = 0;
            Object.keys(attributeObj).forEach(function(key, index){
                if(Array.isArray(attributeObj[key])){
                    stCount = stCount + 1;
                    eanObj['#' + key] = key;
                    eavObj[':' + key] = attributeObj[key];
                    stString = stString + '#' + key + '=' + ':' + key + ',';
                }else if(attributeObj[key] == ''){
                    rmCount = rmCount + 1;
                    eanObj['#' + key] = key;
                    rmString = rmString + '#' + key + ',';
                }else{
                    stCount = stCount + 1;
                    eanObj['#' + key] = key;
                    eavObj[':' + key] = attributeObj[key];
                    stString = stString + '#' + key + '=' + ':' + key + ',';
                }
            });
            if(stCount > 0){
                stString = stString.substring(0, stString.length - 1);//最後のカンマを削除
            }else{
                stString = '';
            }
            if(rmCount > 0){
                rmString = rmString.substring(0, rmString.length - 1);
            }else{
                rmString = '';
            }
            if(stCount > 0 && rmCount > 0){
                ueString = stString + ' ' + rmString;
            }else if(stCount > 0){
                ueString = stString;
            }else if(rmCount > 0){
                ueString = rmString;
            }else{
                resolve();
            }
            let params = {
                TableName: table,
                Key: key,
                ExpressionAttributeNames: eanObj,
                UpdateExpression: ueString
            };
            if(Object.keys(eavObj).length > 0){
                params.ExpressionAttributeValues = eavObj;
            }
            console.log('Uppdate Attribute:', params);
            docClient.update(params, function(err,data){
                if(err){
                    reject(err);
                }else{
                    resolve(data);
                }
            });
        });
    };

    this.deleteAttribute = function (table, key, attributeList){
        //key = {primaryKey: value, sortKey: value}
        //attributeList = ['atteributeA', 'attributeB']
        return new Promise(function(resolve,reject){
            let eanObj = {};
            let ueString = 'REMOVE ';
            attributeList.forEach(function(attribute, index){
                eanObj['#' + attribute] = attribute;
                ueString = ueString + '#' + attribute + ',';
            });
            ueString = ueString.substring(0, ueString.length - 1);//最後のカンマを削除
            const params = {
                TableName: table,
                Key: key,
                ExpressionAttributeNames: eanObj,
                UpdateExpression: ueString
            };
            docClient.update(params, function(err,data){
                if(err){
                    reject(err);
                }else{
                    resolve(data);
                }
            });
        });
    };

}
