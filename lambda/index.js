'use strict';
const AWS = require('aws-sdk');

// Utility
const Utility = require('./Utility');
const util = new Utility();


//DynamoDB
const DBClient = require('./DBClient');
const dbClient = new DBClient();
let GAME = 'DJGame';
let PLAYER = 'DJPlayer';

const gameStatus = {
    waiting: 'waiting',
    setting: 'setting',
    night: 'night',
    day: 'day',
    finished: 'finished'
};

const roleArry = {
    6: ['wolf', 'madman', 'palmist', 'knight', 'citizen', 'citizen'],
    7: ['wolf', 'madman', 'palmist', 'knight', 'citizen', 'citizen', 'citizen'],
    8: ['wolf', 'wolf', 'madman', 'palmist', 'knight', 'citizen', 'citizen', 'citizen'],
    9: ['wolf', 'wolf', 'madman', 'palmist', 'knight', 'citizen', 'citizen', 'citizen', 'citizen'],
    10: ['wolf', 'wolf', 'madman', 'palmist', 'knight', 'mystic','citizen', 'citizen', 'citizen', 'citizen'],
    11: ['wolf', 'wolf', 'madman', 'palmist', 'knight', 'mystic','citizen', 'citizen', 'citizen', 'citizen', 'citizen'],
    12: ['wolf', 'wolf', 'madman', 'palmist', 'knight', 'mystic','citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen'],
    13: ['wolf', 'wolf', 'wolf', 'madman', 'palmist', 'knight', 'mystic','citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen'],
    14: ['wolf', 'wolf', 'wolf', 'madman', 'palmist', 'knight', 'mystic','citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen'],
    15: ['wolf', 'wolf', 'wolf', 'madman', 'palmist', 'knight', 'mystic','citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen'],
    16: ['wolf', 'wolf', 'wolf', 'madman', 'palmist', 'knight', 'mystic','citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen'],
    17: ['wolf', 'wolf', 'wolf', 'madman', 'madman', 'palmist', 'knight', 'mystic','citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen'],
    18: ['wolf', 'wolf', 'wolf', 'madman', 'madman', 'palmist', 'knight', 'mystic','citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen', 'citizen']
};

exports.handler = (event, context, callback) => {
    console.log('RECIEVED EVENT:', event);

    // const isProd = event.context.stage == 'prod' ? true : false;

    Promise.resolve()
    .then(function(){
        const resourcePath = event.context['resource-path'];
        const method = event.context['http-method'];
        switch (resourcePath){
            case '/game':
                if(event.context['cognito-identity-id']){
                    const identityId = event.context['cognito-identity-id'];
                    switch (method) {
                        case 'GET':
                            const gameId = event.params.querystring.gameId;
                            return new Promise(function(resolve, reject){
                                let resultObj = {};
                                dbClient.getItem(PLAYER, {userId: identityId, gameId: gameId})
                                .then(function(player){
                                    console.log('Player:', player);
                                    if(!player){
                                        resolve(resultObj);
                                    }else{
                                        resultObj.player = player;
                                        return Promise.all([
                                            dbClient.getItem(GAME, {gameId: gameId}),
                                            dbClient.query(PLAYER, '', {primaryKey:{ name:'gameId', value: gameId }})
                                        ]);
                                    }
                                })
                                .then(function(result){
                                    console.log('Game/Get:', result);
                                    const game = result[0];
                                    const players = result[1];
                                    game.numOfPlayers = players.length;
                                    if(resultObj.player.role == 'wolf'){
                                        //人狼には、他の人狼が誰であるかを提供
                                        let wolfs = [];
                                        players.forEach(function(player){
                                            if(player.role == 'wolf'){
                                                wolfs.push(player);
                                            }
                                        });
                                        resultObj.wolfs = wolfs;
                                    }else if(resultObj.player.role == 'madman'){
                                        //狂人は特になし
                                    }else if(resultObj.player.role == 'palmist'){
                                        //占い師には特になし
                                    }else if(resultObj.player.role == 'knight'){
                                        //騎士には特になし
                                    }else if(resultObj.player.role == 'mystic'){
                                        //霊媒師には、最も直近で死んだ人の情報を提供
                                    }else if(resultObj.player.role == 'citizen'){
                                        //市民には特になし
                                    }
                                    if(resultObj.player.status == 'dead' || game.gameStatus == gameStatus.finished){
                                        resultObj.players = players
                                    }else{
                                        //roleの情報をフロントに返さない
                                        let alivePlayers = [];
                                        players.forEach(function(player){
                                            if(!player.status || player.status == 'alive'){
                                                const obj = {
                                                    playerName: player.playerName,
                                                    userId: player.userId
                                                };
                                                alivePlayers.push(obj);
                                            }
                                        });
                                        resultObj.players = alivePlayers;
                                    }
                                    resultObj.game = game;
                                    resolve(resultObj);
                                })
                                .catch(function(err){
                                    reject(err);
                                });
                            });
                            break;
                        case 'POST':
                            const newGameId = event.context['request-id'];
                            const ownerName = event['body-json'].name;
                            return new Promise(function(resolve, reject){
                                Promise.all([
                                    dbClient.putItem(GAME, {gameId: newGameId, ownerId: identityId, ownerName: ownerName, gameStatus: gameStatus.waiting}),
                                    dbClient.putItem(PLAYER, {gameId: newGameId, userId: identityId, playerName: ownerName})
                                ])
                                .then(function(){
                                    resolve(newGameId);
                                })
                                .catch(function(err){
                                    reject(err);
                                });
                            });
                        default:
                            throw new Error(`Unsupported method "${event.context['http-method']}"`);
                    }
                }else{
                    throw new Error('No Cognito Identity Id');
                }
                break;
            case '/player':
                if(event.context['cognito-identity-id']){
                    const identityId = event.context['cognito-identity-id'];
                    switch (method) {
                        case 'POST':
                            const gameId = event.params.querystring.gameId;
                            return new Promise(function(resolve, reject){
                                dbClient.getItem(GAME, {gameId: gameId})
                                .then(function(gameData){
                                    console.log(gameData);
                                    if(event['body-json'].name && gameData.gameStatus == gameStatus.waiting){
                                        return dbClient.putItem(PLAYER, {gameId: gameId, userId: identityId, playerName: event['body-json'].name});
                                    }else if(event['body-json'].deletePlayer && gameData.ownerId == identityId && event['body-json'].deletePlayer != gameData.ownerId){
                                        return dbClient.deleteItem(PLAYER, {gameId: gameId, userId: event['body-json'].deletePlayer});
                                    }else if(event['body-json'].action){
                                        const action = event['body-json'].action;
                                        if(action == 'start_game'){
                                            return roleDivide(gameId);
                                        }
                                    }else if(event['body-json'].userId){
                                        //ターゲットの選択
                                        return selectTarget(gameId, identityId, event['body-json'].userId);
                                    }else{
                                        throw new Error('ActionNotAllowed');
                                    }
                                })
                                .then(function(result){
                                    resolve()
                                })
                                .catch(function(err){
                                    reject(err);
                                });
                            });
                        default:
                            throw new Error(`Unsupported method "${event.context['http-method']}"`);
                    }
                }else{
                    throw new Error('No Cognito Identity Id');
                }
                break;
            default:
                throw new Error(`Unsupported resourcePath "${event.context['resource-path']}"`);
        }

    })
    .then(function(result){
        console.log('成功', result);
        callback(null, result);
    })
    .catch(function(err){
        console.log('エラー', err);
        callback(err);
    });
};

/*
プレイ可能人数：6人~18人
6人：人狼1、狂人1、占い師1、騎士1、市民2
7人：人狼1、狂人1、占い師1、騎士1、市民3
8人：人狼2、狂人1、占い師1、騎士1、市民3
9人：人狼2、狂人1、占い師1、騎士1、市民4
10人：人狼2、狂人1、占い師1、騎士1、霊媒師1、市民4
11人：人狼2、狂人1、占い師1、騎士1、霊媒師1、市民5
12人：人狼2、狂人1、占い師1、騎士1、霊媒師1、市民6
13人：人狼3、狂人1、占い師1、騎士1、霊媒師1、市民6
14人：人狼3、狂人1、占い師1、騎士1、霊媒師1、市民7
15人：人狼3、狂人1、占い師1、騎士1、霊媒師1、市民8
16人：人狼3、狂人1、占い師1、騎士1、霊媒師1、市民9
17人：人狼3、狂人2、占い師1、騎士1、霊媒師1、市民9
18人：人狼3、狂人2、占い師1、騎士1、霊媒師1、市民10
*/


function roleDivide(gameId){
    return new Promise(function(resolve, reject){
        dbClient.query(PLAYER, '', {primaryKey:{ name:'gameId', value: gameId }})
        .then(function(players){
            if(players.length >= 6 && players.length <= 18){
                const roles = util.ramdomArry(roleArry[players.length]);
                let promises = [];
                players.forEach(function(player, index){
                    promises.push(dbClient.updateAttribute(PLAYER, {gameId: gameId, userId: player.userId}, {role: roles[index], status: 'alive'}));
                });
                return Promise.all(promises);
            }else{
                throw new Error('プレイ可能人数は6〜18人です。');
            }
        })
        .then(function(){
            return dbClient.updateAttribute(GAME, {gameId: gameId}, {gameStatus: gameStatus.night});
        })
        .then(function(){
            resolve();
        })
        .catch(function(err){
            reject(err);
        });
    });
}

function selectTarget(gameId, userId, targetUserId){
    return new Promise(function(resolve,reject){
        console.log('Select Target');
        let game;
        Promise.all([
            dbClient.getItem(GAME, {gameId: gameId}),
            dbClient.getItem(PLAYER, {gameId: gameId, userId: userId}),
            dbClient.query(PLAYER, '', { primaryKey:{ name:'gameId', value: gameId}, filter:[{ name: 'status', value: 'alive', condition: '=' }] })
        ])
        .then(function(result){
            game = result[0];
            const thisPlayer = result[1]
            const players = result[2];
            let wolfs = [];
            let selectedPlayer;
            players.forEach(function(player){
                if(player.role == 'wolf'){
                    wolfs.push(player.userId);
                }
                if(player.userId == targetUserId){
                    selectedPlayer = player;
                }
            });
            console.log('人狼リスト:', wolfs);
            if(thisPlayer.status == 'dead'){//死んでいる場合、操作できない
                throw new Error('死亡しているため操作できません。');
            }else if(userId == targetUserId){//自分を選択することはできない
                throw new Error('自分を選択することはできません。');
            }else if(thisPlayer.role == 'wolf' && wolfs.indexOf(targetUserId) >= 0){//人狼が人狼を選ぼうとした場合は拒否
                throw new Error('人狼が人狼を殺すことはできません。');
            }else if(thisPlayer.role == 'palmist' && thisPlayer.target && game.gameStatus == gameStatus.night){
                //占い師は二度目の操作はできない
                throw new Error('占い師は二度目の操作はできません。');
            }else{
                let updateObj = {target: targetUserId, targetName: selectedPlayer.playerName};
                if(thisPlayer.role == 'palmist' && game.gameStatus == gameStatus.night){
                    updateObj.palmist = {
                        name: selectedPlayer.playerName,
                        role: selectedPlayer.role == 'wolf' ? 'wolf' : 'human'
                    };
                }
                return dbClient.updateAttribute(PLAYER, {gameId: gameId, userId: userId}, updateObj);
            }
        })
        .then(function(){
            return dbClient.query(PLAYER, '', { primaryKey:{ name:'gameId', value: gameId}, filter:[{ name: 'status', value: 'alive', condition: '=' }] });
        })
        .then(function(result){
            const playersArry = result;
            const num = playersArry.length;
            let complete = 0;
            let allSelection = {};
            let suspiciousSelection = {};
            let wolfSelection = {};
            let knightSelection = '';
            playersArry.forEach(function(player){
                if(player.target){
                    complete = complete + 1;
                    if(allSelection[player.target]){
                        allSelection[player.target] = allSelection[player.target] + 1;
                    }else{
                        allSelection[player.target] = 1;
                    }
                    if(player.role == 'wolf'){
                        if(wolfSelection[player.target]){
                            wolfSelection[player.target] = wolfSelection[player.target] + 1;
                        }else{
                            wolfSelection[player.target] = 1;
                        }
                    }
                    if(player.role == 'knight'){
                        knightSelection = player.target;
                    }
                    if(player.role == 'citizen' || player.role == 'madman' || player.role == 'mystic'){
                        if(suspiciousSelection[player.target]){
                            suspiciousSelection[player.target] = suspiciousSelection[player.target] + 1;
                        }else{
                            suspiciousSelection[player.target] = 1;
                        }
                    }

                }
            });
            console.log('生存者数:', num);
            console.log('操作完了数:', complete);
            if(complete == num){
                console.log('全員操作完了');
                //結果判定ロジック
                if(game.gameStatus == gameStatus.night){
                    //怪しい人を算出する
                    let suspiciousList = [];
                    Object.keys(suspiciousSelection).forEach(function(key){
                        const obj = {
                            userId: key,
                            count: suspiciousSelection[key]
                        };
                        suspiciousList.push(obj);
                    });
                    suspiciousList.sort(util.compareValues('count', 'desc'));//投票数が多い順に並び替え
                    console.log('怪しいと投票されたプレイヤー', suspiciousList);
                    const suspiciousPlayerUserId = suspiciousList[0].userId;
                    //夜の場合は、人狼の投票結果に応じて一人のステータスをDeadにする
                    let selection = [];
                    Object.keys(wolfSelection).forEach(function(key){
                        const obj = {
                            userId: key,
                            count: wolfSelection[key]
                        };
                        selection.push(obj);
                    });
                    selection.sort(util.compareValues('count', 'desc'));//投票数が多い順に並び替え
                    console.log('人狼により投票されたプレイヤー:', selection);
                    const killedPlayerUserId = selection[0].userId;
                    let killedPlayer;
                    let suspiciousPlayer;
                    playersArry.forEach(function(player){
                        if(player.userId == killedPlayerUserId){
                            killedPlayer = player;
                        }
                        if(player.userId == suspiciousPlayerUserId){
                            suspiciousPlayer = player;
                        }
                    });
                    console.log('処刑されるプレイヤー:', killedPlayer);
                    if(killedPlayer.userId == knightSelection){
                        console.log('騎士が守ってゲーム続行');
                        let promises = [];
                        promises.push(dbClient.updateAttribute(GAME, {gameId: gameId}, {gameStatus: gameStatus.day, killed: '', suspicious: suspiciousPlayer.playerName}));//昼に切り替え
                        playersArry.forEach(function(player){
                            promises.push(dbClient.updateAttribute(PLAYER, {gameId: gameId, userId: player.userId}, {target: ''}));//target属性を削除する
                        });
                        return Promise.all(promises);
                    }else{
                        return new Promise(function(innerResolve, innerReject){
                            dbClient.updateAttribute(PLAYER, {gameId: gameId, userId: killedPlayer.userId}, {status: 'dead'})
                            .then(function(){
                                console.log('結果計算');
                                return calcResult(gameId);
                            })
                            .then(function(result){
                                if(result === false){
                                    //ゲーム続行
                                    console.log('ゲーム続行');
                                    let promises = [];
                                    promises.push(dbClient.updateAttribute(GAME, {gameId: gameId}, {gameStatus: gameStatus.day, killed: killedPlayer.playerName, suspicious: suspiciousPlayer.playerName}));//昼と夜の切り替え
                                    playersArry.forEach(function(player){
                                        promises.push(dbClient.updateAttribute(PLAYER, {gameId: gameId, userId: player.userId}, {target: ''}));//target属性を削除する
                                    });
                                    return Promise.all(promises);
                                }else{
                                    return;
                                }
                            })
                            .then(function(){
                                innerResolve();
                            })
                            .catch(function(err){
                                innerReject(err);
                            });
                        });
                    }
                }else if(game.gameStatus == gameStatus.day){
                    //昼の場合は、全員の投票結果に応じて一人のステータスをDeadにする
                    let selection = [];
                    Object.keys(allSelection).forEach(function(key){
                        const obj = {
                            userId: key,
                            count: allSelection[key]
                        };
                        selection.push(obj);
                    });
                    selection.sort(util.compareValues('count', 'desc'));//投票数が多い順に並び替え
                    console.log('投票されたプレイヤー:', selection);
                    const killedPlayerUserId = selection[0].userId;
                    let killedPlayer;
                    let mysticUserId = '';
                    playersArry.forEach(function(player){
                        if(player.userId == killedPlayerUserId){
                            killedPlayer = player;
                        }
                        if(player.role == 'mystic'){
                            mysticUserId = player.userId;
                        }
                    });
                    console.log('処刑されるプレイヤー:', killedPlayer);
                    return new Promise(function(innerResolve, innerReject){
                        let promises = [dbClient.updateAttribute(PLAYER, {gameId: gameId, userId: killedPlayer.userId}, {status: 'dead'})];
                        if(mysticUserId != ''){
                            promises.push(dbClient.updateAttribute(PLAYER, {gameId: gameId, userId: mysticUserId}, {mystic: killedPlayer}));
                        }
                        Promise.all(promises)
                        .then(function(){
                            return calcResult(gameId);
                        })
                        .then(function(result){
                            if(result === false){
                                //ゲーム続行
                                let promises = [];
                                promises.push(dbClient.updateAttribute(GAME, {gameId: gameId}, { gameStatus: gameStatus.night, killed: killedPlayer.playerName}));//昼と夜の切り替え
                                playersArry.forEach(function(player){
                                    promises.push(dbClient.updateAttribute(PLAYER, {gameId: gameId, userId: player.userId}, {target: ''}));//target属性を削除する
                                });
                                return Promise.all(promises);
                            }else{
                                return;
                            }
                        })
                        .then(function(){
                            innerResolve();
                        })
                        .catch(function(err){
                            innerReject(err);
                        });
                    });
                }
            }else{
                console.log('ターン操作未完了');
                return true;
            }
        })
        .then(function(){
            resolve();
        })
        .catch(function(err){
            reject(err);
        });
    });
}

function calcResult(gameId){
    return new Promise(function(resolve,reject){
        console.log('CalcResult');
        dbClient.query(PLAYER, '', { primaryKey:{ name:'gameId', value: gameId}, filter:[{ name: 'status', value: 'alive', condition: '=' }] })//生存者のみ洗い出し
        .then(function(alivePlayers){
            //生存している人狼の数が、全体の生存者の半数になれば人狼の勝ち or 人狼が全滅すれば村人の勝ち
            //判定1：人狼は存在しているか？→判定2：存在している人狼は全体の生存者の半数か？
            let halfNum = alivePlayers.length / 2;
            let wolfs = [];
            alivePlayers.forEach(function(alivePlayer){
                if(alivePlayer.role == 'wolf'){
                    wolfs.push(alivePlayer);
                }
            });
            if(wolfs.length == 0){
                //村人の勝利
                console.log('村人の勝利');
                return dbClient.updateAttribute(GAME, {gameId: gameId}, {gameStatus: gameStatus.finished, winner: 'humans'});
            }else if(wolfs.length >= halfNum){
                //人狼の勝利
                console.log('人狼の勝利');
                return dbClient.updateAttribute(GAME, {gameId: gameId}, {gameStatus: gameStatus.finished, winner: 'wolfs'});
            }else{
                //ゲーム続行
                console.log('ゲーム続行');
                return false;
            }
        })
        .then(function(result){
            if(result === false){
                resolve(false);
            }else{
                resolve(true);
            }
        })
        .catch(function(err){
            reject(err);
        });
    });
}